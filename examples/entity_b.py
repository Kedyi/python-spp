import spp


udp_transport = spp.UdpTransport()

config = spp.Config(
    my_apid=333,
    routing={
        (111, None): (udp_transport, "localhost:5222"),
        (333, None): (udp_transport, "localhost:5333"),
        },
    )

spp_entity = spp.SpacePacketProtocolEntity(config)
spp_entity.bind()

input("Running. Press <Enter> to send packet...\n")

space_packet = spp.SpacePacket(
    packet_type=spp.PacketType.TELEMETRY,
    packet_sec_hdr_flag=False,
    apid=111,
    sequence_flags=spp.SequenceFlags.UNSEGMENTED,
    packet_sequence_count=1,
    packet_data_field=b"This is a test packet!"
    )

spp_entity.request(
    space_packet,
    space_packet.apid,
    apid_qualifier=None,
    qos=None,
    )

input("Press <Enter> to stop...\n")
spp_entity.unbind()
