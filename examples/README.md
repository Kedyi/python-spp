
This example routes a packet from A to B over a waypoint, and another packet
back from B to A over the same waypoint.

Start all entities and the waypoint:

```
$ python entity_a.py
```

```
$ python waypoint.py
```

```
$ python entity_b.py
```

Then press <Enter> on Entity A to send out a packet, and then do the same
on Entity B.
