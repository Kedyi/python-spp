import socket
import threading
import select

from .base import Transport


DEFAULT_MAXIMUM_PACKET_LENGTH = 4096


class UdpTransport(Transport):

    def __init__(self, maximum_packet_length=DEFAULT_MAXIMUM_PACKET_LENGTH):
        super().__init__()
        self._maximum_packet_length = maximum_packet_length
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._thread = threading.Thread(target=self._incoming_pdu_handler)
        self._thread.kill = False

    def bind(self, address):
        host, port = address.split(":")
        port = int(port)
        self._socket.bind((host, port))
        self._thread.start()

    def unbind(self):
        self._thread.kill = True
        self._thread.join()
        self._socket.close()

    def request(self, pdu, address):
        host, port = address.split(":")
        port = int(port)
        self._socket.sendto(pdu, (host, port))

    def _incoming_pdu_handler(self):
        thread = threading.currentThread()
        _socket_list = [self._socket]
        buffer_size = 10 * self._maximum_packet_length

        while not thread.kill:
            try:
                readable, _, _ = select.select(_socket_list, [], [], 0)
            except ValueError:
                pass

            for sock in readable:
                pdu, addr = self._socket.recvfrom(buffer_size)
                self.indication(pdu, addr)
