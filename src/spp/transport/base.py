

class Transport:

    def __init__(self):
        self.config = None

    def connect(self, *args, **kwargs):
        pass

    def disconnect(self, *args, **kwargs):
        pass

    def bind(self, *args, **kwargs):
        pass

    def unbind(self, *args, **kwargs):
        pass

    def request(self, data, address):
        raise NotImplementedError

    def indication(self, data, address=None):
        pass

    def shutdown(self):
        pass
