from .space_packet import SpacePacket


class Config:

    def __init__(
            self,
            my_apid=None,
            packet_type_of_outgoing_packets=None,
            routing={},
            ):
        self.my_apid = my_apid
        self.packet_type_of_outgoing_packets = packet_type_of_outgoing_packets
        self.routing = routing


class SpacePacketProtocolEntity:

    def __init__(self, config):
        self._config = config

        for transport, address in self._config.routing.values():
            transport.indication =\
                lambda pdu, address: self._pdu_received(pdu, address)

    def bind(self):
        if self._config.my_apid is None:
            raise ValueError("Own APID not defined")
        apid = self._config.my_apid
        apid_qualifier = None
        transport, address = self._config.routing.get((apid, apid_qualifier))
        transport.bind(address)

    def unbind(self):
        apid = self._config.my_apid
        apid_qualifier = None
        transport, address = self._config.routing.get((apid, apid_qualifier))
        transport.unbind()

    def request(self, space_packet, apid, apid_qualifier=None, qos=None):
        if (apid, apid_qualifier) not in self._config.routing:
            raise ValueError(
                "No routing defined for {}".format((apid, apid_qualifier)))
        transport, address = self._config.routing.get((apid, apid_qualifier))
        transport.request(space_packet.encode(), address)

    def indication(self, space_packet, apid, apid_qualifier=None):
        # to be overwritten by higher layer user
        pass

    def _pdu_received(self, pdu, address):
        space_packet = SpacePacket.decode(pdu)
        if self._config.my_apid is not None\
                and self._config.my_apid == space_packet.apid:
            print("Received Space Packet:", space_packet)
            attrs = vars(space_packet)
            print(', '.join("%s: %s" % item for item in attrs.items()))
            self.indication(space_packet, space_packet.apid)
        else:
            print("Forwarding Space Packet:", space_packet)
            self.request(space_packet, space_packet.apid)
